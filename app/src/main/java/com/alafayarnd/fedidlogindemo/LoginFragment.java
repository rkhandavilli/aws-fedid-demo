package com.alafayarnd.fedidlogindemo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alafayarnd.moneris.MonerisVaultClient;
import com.alafayarnd.moneris.model.Empty;
import com.alafayarnd.moneris.model.VaultResponse;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.apigateway.ApiClientFactory;
import com.amazonaws.regions.Regions;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.HashMap;
import java.util.Map;


public class LoginFragment extends Fragment {

    CallbackManager callbackManager;
    private LoginButton loginButton;
    private TextView loginStatus;


    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        loginButton = (LoginButton) view.findViewById(R.id.login_button);
        loginButton.setFragment(this);

        loginStatus = (TextView) view.findViewById(R.id.login_status);
        // Inflate the layout for this fragment

        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Access Token: ", loginResult.getAccessToken().getToken());
                loginStatus.setText(loginResult.getAccessToken().getToken());

                GetAWSCredentials task = new GetAWSCredentials();
                task.execute();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

                loginStatus.setText(error.toString());
            }
        });

        Button refreshButton = (Button) view.findViewById(R.id.refresh_button);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetAWSCredentials task = new GetAWSCredentials();
                task.execute();

//                GetARNRole task2 = new GetARNRole();
//                task2.execute();

            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private class GetAWSCredentials extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                    getActivity().getApplicationContext(), // Context
                    "us-east-1:5a2fbc59-3b72-431d-8a87-9f7eda44b467", // Identity Pool ID
                    Regions.US_EAST_1 // Region
            );

            Map<String, String> logins = new HashMap<>();
            logins.put("graph.facebook.com", AccessToken.getCurrentAccessToken().getToken());
            Log.d("Access Token: ", AccessToken.getCurrentAccessToken().getToken());
            credentialsProvider.setLogins(logins);
            Log.d("identityID: " , credentialsProvider.getIdentityId());
            AWSCredentials credentials = credentialsProvider.getCredentials();
            Log.d("Access key id: " , credentials.getAWSAccessKeyId());
            Log.d("Secret key: " , credentials.getAWSSecretKey());
            ApiClientFactory factory = new ApiClientFactory().credentialsProvider(credentialsProvider);
            // create a client
            final MonerisVaultClient client = factory.build(MonerisVaultClient.class);

            // Invoke your creditcardGet method
            VaultResponse output = client.creditcardGet();

            Log.d("Output: ", output.getStatus());

            // you also have access to your API models
            Empty myModel = new Empty();

            return null;
        }
    }

    private class GetARNRole extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                    getActivity().getApplicationContext(), // Context
                    "us-east-1:5a2fbc59-3b72-431d-8a87-9f7eda44b467", // Identity Pool ID
                    Regions.US_EAST_1 // Region
            );

            Map<String, String> logins = new HashMap<>();
            logins.put("graph.facebook.com", AccessToken.getCurrentAccessToken().getToken());

            Log.d("ARN Role: " , credentialsProvider.getCustomRoleArn());

            return null;
        }
    }
}
